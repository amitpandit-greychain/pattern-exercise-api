# Patterns-Exercise

# Setup Development environment

## Environment Config

The api.env file is committed in code repo.

## Start API server and all dependent services

---
**NOTE**
**It will run on containerized enviroment so Docker and Docker-compose must be installed on the system.**

---
Simply Run the follwing commands from root directory.

```
$ npm i

$ ./up-back.sh
OR
$ docker network create patterns-network
$ docker-compose up
```

This will run the postgres, ptlab-crud-api containers locally.

---
**NOTE**
**To Access the Database using any GUI took like, https://dbeaver.io/ Or https://www.pgadmin.org/  Following are the credentials to connect Postgers DB (as we are using postgres docker container) **
```
HOST=localhost
PORT=5432
USERNAME=ptlabexuser
PASSWORD=ptlabexpwd
DB_NAME=ptlabexedb
```
---


# URLs

Swagger Endpoint: GET http://localhost:3000/api-docs

## TechStack

_Language:_ TypeScript  

_Platform:_ Node.js  

_ORM:_ TypeORM

_Framework:_ Express

_Database:_ PostgreSQL

_Package management:_ NPM

import "mocha";
import { expect } from "chai";
import { agent as request } from "supertest";

describe("Users API", () => {
  let jobId: number;
  const testPayload = {
    title: "Welder for Workshop",
    description: "Job descriptions",
    location: {
      country: "USA",
      state: "Minneapolis",
      city: "Bloomington",
      street: "Street 33D",
      zip: "47405",
      lat: 88.2,
      long: 10.11,
    },
    hour_rate: 15,
    work_hr_start: "10:00AM",
    work_hr_end: "08:00PM",
    job_type: "Full-Time",
  };

  before(async () => {
    const res = await request("http://localhost:3000")
      .post("/api/v1/job")
      .send(testPayload);
    jobId = res.body.response.dataset.id;
  });

  after(async () => {
    await request("http://localhost:3000").delete(`/api/v1/job/${jobId}`);
  });

  describe("GET api/v1/jobs", () => {
    it("should get all Jobs", async () => {
      const res = await request("http://localhost:3000").get(
        "/api/v1/jobs?page=1&limit=1"
      );
      expect(res.status).to.equal(200);
    });
  });

  describe("GET api/v1/job/:id([0-9]+)", () => {
    it("should get user", async () => {
      const res = await request("http://localhost:3000").get(
        `/api/v1/job/${jobId}`
      );
      expect(res.status).to.equal(200);
    });
  });
});

import * as Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { httpResponse } from "../utils/responseHandler";

interface jobRequest extends Request {
  value?: { body?: string };
}
export class JobValidator {
  constructor() {}
  // default request payload considered to be Body
  validatePayload(schema: any, prop: string = "body") {
    return async (req: jobRequest, res: Response, next: NextFunction) => {
      try {
        let requestPayload;
        switch (prop) {
          case "body":
            requestPayload = req.body;
            break;
          case "params":
            requestPayload = req.params;
            break;
          case "query":
            requestPayload = req.query;
            break;
          default:
            requestPayload = req.body;
            break;
        }
        const val = await schema.validateAsync(requestPayload, {
          convert: true,
          errors: {
            wrap: {
              label: "",
            },
          },
          abortEarly: false,
        });
        req.value = req.value ?? {};
        req.value.body = req.value.body ?? val;
        next();
      } catch (error: any) {
        const { details } = error;
        httpResponse.handleError(res, details[0].message.replace(/_/g, " "));
      }
    };
  }
}

export const jobSchema = Joi.object().keys({
  title: Joi.string().trim().required(),
  description: Joi.string().trim().required(),
  hour_rate: Joi.number().precision(2).required(),
  work_hr_start: Joi.string()
    .pattern(
      new RegExp("([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])s*([AaPp][Mm])")
    )
    .required(),
  work_hr_end: Joi.string()
    .pattern(
      new RegExp("([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])s*([AaPp][Mm])")
    )
    .required(),
  location: Joi.object().keys({
    country: Joi.string().trim().required(),
    state: Joi.string().trim().required(),
    city: Joi.string().trim().required(),
    street: Joi.string().trim().required(),
    zip: Joi.string().alphanum().trim().required(),
    lat: Joi.number().min(-90).max(90).required(),
    long: Joi.number().min(-180).max(180).required(),
  }),
  job_type: Joi.string().valid("Full-Time", "Part-Time", "Casual").required(),
});

export const jobIdSchema = Joi.object({
  id: Joi.number().required(),
});

export const paginateSchema = Joi.object({
  limit: Joi.number().min(0).max(50).optional(), // Results set limit can be changed here.
  page: Joi.number().min(1).optional(),
});

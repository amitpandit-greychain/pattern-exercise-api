import { EntityRepository, Repository } from "typeorm";
import { JobActivityEntity } from "../database/entities/jobsActivity";

@EntityRepository(JobActivityEntity)
export class JobActivityRespository extends Repository<JobActivityEntity> {}

import { EntityRepository, Repository } from "typeorm";
import { JobEntity } from "../database/entities/jobs.entity";

@EntityRepository(JobEntity)
export class JobRepository extends Repository<JobEntity> {}

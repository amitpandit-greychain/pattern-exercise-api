import { Router } from "express";
import JobController from "../controller/job.controller";
import {
  JobValidator,
  jobSchema,
  jobIdSchema,
  paginateSchema,
} from "../validators/jobValidators";

class JobPostRoutes {
  router = Router();
  _jobCtrl = new JobController();
  _jobValidators = new JobValidator();
  constructor() {
    this.intializeRoutes();
  }
  intializeRoutes() {
    this.router
      .route("/jobs")
      .get(
        this._jobValidators.validatePayload(paginateSchema, "query"),
        this._jobCtrl.index
      );
    this.router
      .route("/job")
      .post(
        this._jobValidators.validatePayload(jobSchema),
        this._jobCtrl.create
      );
    this.router
      .route("/job/:id")
      .get(
        this._jobValidators.validatePayload(jobIdSchema, "params"),
        this._jobCtrl.getJobById
      );
    this.router
      .route("/job/:id")
      .put(
        this._jobValidators.validatePayload(jobIdSchema, "params"),
        this._jobValidators.validatePayload(jobSchema),
        this._jobCtrl.update
      );
    this.router
      .route("/job/:id")
      .delete(
        this._jobValidators.validatePayload(jobIdSchema, "params"),
        this._jobCtrl.delete
      );
  }
}
export default new JobPostRoutes().router;

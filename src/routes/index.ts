import { Application } from "express";
import jobPostRoutes from "./jobPostRoutes";
import swaggerUi from "swagger-ui-express";
import * as path from "path";
import * as fs from "fs";
export default class Routes {
  constructor(app: Application) {
    const swaggerFile: any = process.cwd() + "/public/swagger.json";
    const swaggerData: any = fs.readFileSync(swaggerFile, "utf8");
    const swaggerDocument = JSON.parse(swaggerData);
    app.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument, {
        explorer: false,
        customSiteTitle: "Patterns-Exercise",
      })
    );

    app.use("/api/v1", jobPostRoutes);

    app.use("*", (req, res) => {
      res.status(404).json({
        errors: {
          msg: "Invalid Endpoint",
        },
      });
    });
  }
}

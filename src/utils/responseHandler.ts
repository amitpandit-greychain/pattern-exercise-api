import { Response } from "express";

/**
 * @author Amit
 * Method to Handle response,
 */
export class httpResponse {
  static handleSuccess = (res: Response, dataset: any, message: string) => {
    res.status(200).json({
      response: {
        status: true,
        message: message,
        dataset: dataset,
      },
    });
  };
  static handleError = (res: Response, message: string) => {
    res.status(500).json({
      response: {
        status: false,
        message: message,
      },
    });
  };
  static InvalidRequest = (res: Response, message: string) => {
    res.status(400).json({
      response: {
        status: false,
        message: message,
      },
    });
  };
  static NotFound = (res: Response, message: string = "") => {
    res.status(404).json({
      response: {
        status: false,
        message: message || "No results found",
        dataset: {},
      },
    });
  };
}

import express, { Request, Response } from "express";
import * as path from "path";
import dotenv from "dotenv";
import { createConnection } from "typeorm";
import Locals from "./config/Locals";
import Routes from "./routes";
class Server {
  private app: express.Application;

  constructor() {
    dotenv.config({ path: path.join(__dirname, "../api.env") });
    this.app = express(); // init the application
    this.configuration();
    this.initDb();
    this.routes();
  }

  /**
   * Method to configure the server,
   * If we didn't configure the port into the environment
   * variables it takes the default port 3000
   */
  public configuration() {
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(express.json());
    this.app.use(express.static("public"));
  }

  /**
   * Init DB connection
   */
  public async initDb() {
    await createConnection(Locals.config().dbConfig);
  }

  /**
   * Method to configure the routes
   */
  public async routes() {
    new Routes(this.app);
  }

  /**
   * Used to start the server
   */
  public start() {
    this.app.listen(this.app.get("port"), () => {
      console.log(`Server is listening ${this.app.get("port")} port.`);
    });
  }
}

const server = new Server(); // Create server instance
server.start(); // Execute the server

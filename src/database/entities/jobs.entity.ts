import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Index,
} from "typeorm";
import { LocationProperties } from "../types/location";
export type jobType = "Full-Time" | "Part-Time" | "Casual"; // we could also keep job type in other table and map id here.

@Entity("jobs")
export class JobEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  title: string;

  @Column({ type: "varchar" })
  description: string;

  @Column({
    type: "jsonb",
  })
  location: LocationProperties;

  @Index({ spatial: true })
  @Column({
    type: "point",
    nullable: true,
  })
  geo_location: String;

  @Column({ type: "numeric" })
  hour_rate: number;

  @Column({ length: 10 })
  work_hr_start: string;

  @Column({ length: 10 })
  work_hr_end: string;

  @Column({ default: true })
  is_active: Boolean;

  @Column({
    type: "enum",
    enum: ["Full-Time", "Part-Time", "Casual"],
    default: "Full-Time",
  })
  job_type: jobType;

  @CreateDateColumn({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP(6)",
  })
  created_at: Date;

  @UpdateDateColumn({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP(6)",
  })
  updated_at: Date;

  @Column({ default: false })
  is_deleted: boolean;

  @DeleteDateColumn({
    type: "timestamp",
    default: null,
  })
  deleted_at: Date;

  @Column({ name: "created_by" })
  created_by?: number;
}

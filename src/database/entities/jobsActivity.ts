import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

export type activityType = "created" | "modified" | "deleted";

@Entity("jobsActivity")
export class JobActivityEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id?: number;

  @Column()
  job_id: number;

  @Column({ type: "varchar" })
  activity_note: string;

  @Column({
    type: "jsonb",
    nullable: true,
  })
  previous_detail_b: any;

  @Column({
    type: "enum",
    enum: ["created", "modified", "deleted"],
  })
  action: activityType;

  @Column({
    type: "jsonb",
    nullable: true,
  })
  current_detail_b: any;

  @CreateDateColumn({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP(6)",
  })
  created_at: Date;
}

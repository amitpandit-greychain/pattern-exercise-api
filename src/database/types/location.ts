export interface LocationProperties {
  country: string;
  state: string;
  city: string;
  street: string;
  zip: string;
  lat: number;
  long: number;
}

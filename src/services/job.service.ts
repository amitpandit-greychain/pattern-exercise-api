import { getConnection, getCustomRepository } from "typeorm";
import { JobEntity } from "../database/entities/jobs.entity";
import { JobRepository } from "./../repository/job.repository";

export class JobService {
  /**
   * Service for get Job list using pagination, can be enhanced with Filter and search query
   */

  public index = async (query: any) => {
    const skip = query.skip;
    const take = query.take;

    const _jobRepository = getCustomRepository(JobRepository);
    const [result, total] = await _jobRepository.findAndCount({
      take: take,
      skip: skip,
    });
    return {
      result,
      count: result.length ? total : 0,
    };
  };
  /**
   * Service for get Job By ID
   */
  public findById = async (id: number) => {
    const _jobRepository = getCustomRepository(JobRepository);
    const job = await _jobRepository.findOne(id);
    return job;
  };
  /**
   * Service for get find single Job using any custom condition
   */
  public findByConditions = async (query: object) => {
    const _jobRepository = getCustomRepository(JobRepository);
    const job = await _jobRepository.findOne({ where: query });
    return job;
  };

  /**
   * Service for create a Job Post
   */
  public create = async (post: JobEntity) => {
    const _jobRepository = getCustomRepository(JobRepository);
    const newPost = await _jobRepository.save(post);
    return newPost;
  };

  /**
   * Service for update a Job by Id
   */
  public update = async (post: JobEntity, id: number) => {
    const _jobRepository = getCustomRepository(JobRepository);
    const updatedPost = await _jobRepository.update(id, post);
    return updatedPost;
  };
  /**
   * Service for Hard delete any Job
   */
  public delete = async (id: number) => {
    const _jobRepository = getCustomRepository(JobRepository);
    const deletedPost = await _jobRepository.delete(id);
    return deletedPost;
  };
  /**
   * Service for Mark softDelete a Job
   */
  public softDelete = async (id: number) => {
    const _jobRepository = getCustomRepository(JobRepository);
    const deletedPost = await _jobRepository.update(id, {
      is_deleted: true,
      is_active: false,
      deleted_at: new Date(),
    });
    return deletedPost;
  };
}

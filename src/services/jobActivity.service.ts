import { getConnection, getCustomRepository } from "typeorm";
import { JobActivityEntity } from "../database/entities/jobsActivity";
import { JobActivityRespository } from "./../repository/jobActivity.repository";

export class JobActivityService {
  public create = async (activity: JobActivityEntity) => {
    const _JobActivityRespository = getCustomRepository(JobActivityRespository);
    const jobActivity = await _JobActivityRespository.save(activity);
    return jobActivity;
  };
}

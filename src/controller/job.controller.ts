import { Router, Response, Request } from "express";
import { JobService } from "../services/job.service"; // import service
import { JobActivityService } from "../services/jobActivity.service";
import { JobEntity } from "../database/entities/jobs.entity";
import Log from "../middlewares/Log";
import { httpResponse } from "../utils/responseHandler";
import {
  JobActivityEntity,
  activityType,
} from "../database/entities/jobsActivity";
export default class JobController {
  private _jobService: JobService;
  private _jobActivityService: JobActivityService;
  constructor() {
    this._jobService = new JobService();
    this._jobActivityService = new JobActivityService();
  }

  /*
   * --------------------------------------------------------------------------
   * @ Function Name            : index()
   * @ Added Date               : 15-11-21
   * @ Added By                 : @mit
   * --------------------------------------------------------------------------
   * @ Description              : Fetch Jobs list
   * --------------------------------------------------------------------------
   * @ return                   : response json format
   * --------------------------------------------------------------------------
   */
  public index = async (req: Request, res: Response) => {
    try {
      const option = {
        skip: Number(req.query.limit) * (Number(req.query.page) - 1),
        take: Number(req.query.limit),
      };
      const posts = await this._jobService.index(option);
      httpResponse.handleSuccess(res, posts, "success");
    } catch (error: any) {
      Log.error(error.message || "Internal Server Error");
      httpResponse.handleError(res, error.message || "Internal Server Error");
    }
  };

  /*
   * --------------------------------------------------------------------------
   * @ Function Name            : getJobById()
   * @ Added Date               : 15-11-21
   * @ Added By                 : @mit
   * --------------------------------------------------------------------------
   * @ Description              : Get Job Details By Id
   * --------------------------------------------------------------------------
   * @ return                   : response json format
   * --------------------------------------------------------------------------
   */

  public getJobById = async (req: Request, res: Response) => {
    try {
      const id = req["params"]["id"];
      const jobDetail = await this._jobService.findById(Number(id));
      const dataset = jobDetail || {};
      if (jobDetail) {
        httpResponse.handleSuccess(res, dataset, "Success");
      } else {
        return httpResponse.NotFound(res);
      }
    } catch (error: any) {
      Log.error(error.message || "Internal Server Error");
      httpResponse.handleError(res, error.message || "Internal Server Error");
    }
  };
  /*
   * --------------------------------------------------------------------------
   * @ Function Name            : create()
   * @ Added Date               : 15-11-21
   * @ Added By                 : @mit
   * --------------------------------------------------------------------------
   * @ Description              : Create new JOB Post
   * --------------------------------------------------------------------------
   * @ return                   : response json format
   * --------------------------------------------------------------------------
   */

  public create = async (req: Request, res: Response) => {
    try {
      const post = req["body"] as JobEntity;
      // KEEPING created_by HARDCODE, AS WE WILL BE EXTRACTING THE USER INFORMATION FROM AUTHENTICATION/JTW, Job Creator
      const jobPayload = { ...post, created_by: 2 };
      jobPayload.geo_location = `${post.location.long} , ${post.location.lat}`;
      const newPost = await this._jobService.create(jobPayload);
      if (newPost) {
        // save activity on create job
        await this.logActivity(newPost.id, jobPayload, null, "created");
      }
      httpResponse.handleSuccess(res, newPost, "Job created successfully");
    } catch (error: any) {
      Log.error(error.message || "Internal Server Error");
      httpResponse.handleError(res, error.message || "Internal Server Error");
    }
  };

  /*
   * --------------------------------------------------------------------------
   * @ Function Name            : update()
   * @ Added Date               : 15-11-21
   * @ Added By                 : @mit
   * --------------------------------------------------------------------------
   * @ Description              : Update/Modify any Job
   * --------------------------------------------------------------------------
   * @ return                   : response json format
   * --------------------------------------------------------------------------
   */

  public update = async (req: Request, res: Response) => {
    try {
      const post = req["body"] as JobEntity;
      const id = req["params"]["id"];
      const jobDetail = await this._jobService.findById(Number(id));
      const updatedJob = await this._jobService.update(post, Number(id));
      if (updatedJob) {
        // save activity on update job
        await this.logActivity(Number(id), post, jobDetail, "modified");
      }
      httpResponse.handleSuccess(res, post, "Job updated successfully");
    } catch (error: any) {
      Log.error(error.message || "Internal Server Error");
      httpResponse.handleError(res, error.message || "Internal Server Error");
    }
  };

  /*
   * --------------------------------------------------------------------------
   * @ Function Name            : delete()
   * @ Added Date               : 15-11-21
   * @ Added By                 : @mit
   * --------------------------------------------------------------------------
   * @ Description              : Delete any Job  (WE CAN SOFT/HARD DELETE THE RECORD. NOT USING HARD DELETE METHOD,
                                 DEPENDING UPON BUSINESS REQUIRMENTS)
   * --------------------------------------------------------------------------
   * @ return                   : response json format
   * --------------------------------------------------------------------------
   */

  public delete = async (req: Request, res: Response) => {
    try {
      const id = req["params"]["id"];
      const findJob = await this._jobService.findByConditions({
        id,
        is_deleted: false,
      });
      if (findJob) {
        // Performing Soft delete here
        await this._jobService.softDelete(Number(id));
        await this.logActivity(Number(id), null, null, "deleted");
        httpResponse.handleSuccess(res, null, "Job deleted successfully");
      } else {
        httpResponse.InvalidRequest(res, "Job does not exist");
      }
    } catch (error: any) {
      Log.error(error.message || "Internal Server Error");
      httpResponse.handleError(res, error.message || "Internal Server Error");
    }
  };

  /*
   * --------------------------------------------------------------------------
   * @ Function Name            : logActivity()
   * @ Added Date               : 15-11-21
   * @ Added By                 : @mit
   * --------------------------------------------------------------------------
   * @ Description              : Track Job activity on Create/update/delete
   * --------------------------------------------------------------------------
   * @ return                   : void
   * --------------------------------------------------------------------------
   */

  public logActivity = async (
    jobId: number,
    CurrentjobPayload: any,
    previousJobPayload: any,
    type: activityType
  ) => {
    // We could choose other's data to log job activity depending on requirement
    const activityPayload = {
      user_id: 1, // KEEPING THIS HARDCODE, AS WE WILL BE EXTRACTING THE USER/ADMIN INFORMATION FROM AUTHENTICATION/JTW
      job_id: jobId,
      action: type,
      activity_note: `Job is ${type}`,
      previous_detail_b: previousJobPayload ? previousJobPayload : {},
      current_detail_b: CurrentjobPayload,
      created_at: new Date(),
    } as JobActivityEntity;
    await this._jobActivityService.create(activityPayload);
  };
}

FROM node:14.15.4
WORKDIR /app
COPY package*.json ./

RUN npm install -g nodemon
RUN npm ci
COPY . .
EXPOSE 3000